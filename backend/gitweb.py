from flask import Flask
import libgitana as lg
import json
from flask import request
from datetime import datetime
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient()
db = client.test
commits = db.commits

def addSort(query, sort):
    if sort:
        if sort == "asc":
            s = 1
        elif sort == "desc":
            s = -1
        else:
            return

        query.append({"$sort": {"count": s}})

def addDateRange(query, before, after):

    if before:
        query.append({"$match": {"time": {"$lte": before}}})

    if after:
        query.append({"$match": {"time": {"$gte": after}}})

def makeQuery(wanted, groupBy):
    name = request.args.get("name")
    sort = request.args.get("sort")
    before = datetime.strptime(request.args.get("before"), '%Y-%m-%d').timestamp() if request.args.get("before") else None
    after = datetime.strptime(request.args.get("after"), '%Y-%m-%d').timestamp()  if request.args.get("after") else None

    groups = {
        "kdeconnect": ["kdeconnect-kde", "kdeconnect-android"],
        "plasma": ["plasma-desktop", "plasma-workspace", "kdeplasma-addons", "kwin", "plasma-pa", "plasma-nm"],
        "itinerary-meta": ["itinerary", "kitinerary", "kpkpass", "kitinerary-workbench"]
    }

    if name in groups:
        p1 = {"$in": groups[name]}
    else:
        p1 = name

    query = []

    addDateRange(query, before, after)

    if name == "kde":
        query.append({"$match": {}})
    else:
        query.append({"$match": {wanted: p1}})

    query.append({"$group": {"_id": "$" + groupBy, "count": {"$sum": 1}}})
    query.append({"$addFields": {groupBy: "$_id"}})
    query.append({"$project": {"_id": 0}})

    addSort(query, sort)

    return commits.aggregate(query)

@app.route("/repo")
def repo():

    result = makeQuery("repo", "author")

    return json.dumps(list(result))

@app.route("/author")
def author():
    result = makeQuery("author", "repo")

    return json.dumps(list(result))
