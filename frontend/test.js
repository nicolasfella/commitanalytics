const app = new Vue({
  el: '#app',
  data: {
    name: "",
    commits: [],
    byAuthor: false
  },
  created () {
//     fetchRepo(this.name)
  },
  methods: {
    fetchIt: function() {
        app.name = document.querySelector("#searchBox").value
//         document.querySelector("#searchBox").value = ""

        date = ""

        before = document.getElementById("before").value
        if (before) {
            date += "&before=" + before
        }

        after = document.getElementById("after").value
        if (after) {
            date += "&after=" + after
        }

        console.log(date)

        url = encodeURI('http://localhost:5000/' + (app.byAuthor ? 'author' : 'repo') + '?name=' + app.name + '&sort=desc' + date)

        fetch(url)
        .then(response => response.json())
        .then(myJson => {
            app.commits = myJson
        });
    }
  }
})

