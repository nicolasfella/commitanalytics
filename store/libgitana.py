from pygit2 import Repository, discover_repository, GIT_SORT_TOPOLOGICAL, GIT_SORT_REVERSE
import os
import functools
import json
import operator
from datetime import datetime

def repoPrettyName(repo):
    return repo.path.split("/")[-3]

def discover_repos(basePath):
    dirs = os.listdir(basePath)

    repoPaths = list(map(lambda f: basePath + "/" + f, dirs))

    return filter(lambda x: x != None, map(findRepo, repoPaths))

def findRepo(path):
    repoPath = discover_repository(path)

    if repoPath == None:
        return None

    return Repository(repoPath)
