from pygit2 import Repository, discover_repository, GIT_SORT_TOPOLOGICAL, GIT_SORT_REVERSE, GitError
from pymongo import MongoClient
import libgitana as lg

def storeRepo(repo, collection):
    name = lg.repoPrettyName(repo)
    buf = []

    try:
        for commit in repo.walk(repo.head.target, GIT_SORT_TOPOLOGICAL):
            buf.append({
                "author": commit.author.name,
                "time": commit.commit_time,
                "repo": name,
                "id": str(commit.id)
            })

            if len(buf) == 1000:
                collection.insert_many(buf)
                buf = []

        collection.insert_many(buf)
    except GitError as e:
        print("Error" + e)
        pass
    except KeyError as e:
        print("KeyError")
        pass

def storeAll(basePath):
    client = MongoClient()
    db = client.test
    repos = lg.discover_repos(basePath)

    #l = len(list(repos))

    #print("repos found: " + str(l))

    i = 1

    #print(list(repos))

    for r in repos:
        name = lg.repoPrettyName(r)
        print("Store " + name + " (" + str(i) + ")")
        storeRepo(r, db.commits)
        i += 1

storeAll("/home/nico/kde/src")
